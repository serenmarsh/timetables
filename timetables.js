//Seren
//15/03
//1001861

//declaring variables
let userNum = 0
let startNum = 1
let endNum = 12

userNum = Number(prompt("Which times table would ypu like to see?"))

//for loop
console.log("--> for loop <--");
for ( i = startNum; i <= endNum; i++) {
    console.log(`${i} x ${userNum} = ${i * userNum}` );
}

//while loop
console.log("--> while loop <--");
i = startNum;
while (i <= endNum) {
    console.log(`${i}    ${i * 10}       ${i * 100}      ${i * 1000}` );
    i++
}